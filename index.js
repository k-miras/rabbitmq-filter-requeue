const amqp = require("amqplib");
const bunyan = require("bunyan");
const config = require("config");

const { remoteRabbit } = require("./singletone");

const log = bunyan.createLogger({
  name: "main"
});

const main = () => {
  remoteRabbit
    .then(ch => {
      log.info("Successfully created channel");

      cache = {};

      const q = config.get("READ_Q");
      const x = config.get("INPUT_X");
      const inputk = config.get("INPUT_ROUTING_KEY");
      ch.prefetch(config.get("PREFETCH_COUNT"));
      log.info("Settings - OK\nStarting consume");
      ch.consume(q, message => {
        const msg = JSON.parse(message.content.toString());
        log.info(`Received: ${msg}`);

        if (cache[msg.id] === undefined) {
          log.info(`requeue: ${msg}`);
          ch.publish(x, inputk, new Buffer(msg));
        } else {
          log.info(`drop: ${msg}`);
        }

        ch.ack(message);
      });
    })
    .catch(err => {
      log.error(err, "There was an error while working with RabbitMQ server");
    });
};

main();
