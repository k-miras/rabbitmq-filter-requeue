const amqp = require("amqplib");
const config = require("./config");

let remote = amqp.connect(config.REMOTE_CONNECION_URL).then(conn => {
  console.log(
    `Successfully connected to RabbitMQ server: ${config.REMOTE_CONNECION_URL}`
  );
  return conn.createChannel();
});

module.exports = remote;
